var urlVar = window.location.search;
var list = (urlVar.substr(1)).split('&');
var stoptime = true;

const labels = [
'The Pawn',
'The Bishop',
'The Knight',
'The Rook',
'The Queen',
'The King',
];

const data = {
labels: labels,
datasets: [{
  label: 'Chess items',
  backgroundColor: '#758C51',
  borderColor: '#758C51',
  data: list,
}]
};

const config = {
type: 'line',
data: data,
options: {}
};

const myChart = new Chart(
document.getElementById('myChart'),
config
);


function showChart()
{

	let pawn = document.getElementsByName("pawn").length,
	bishop = document.getElementsByName("bishop").length,
	knight = document.getElementsByName("knight").length,
	rook = document.getElementsByName("rook").length,
	queen = document.getElementsByName("queen").length,
	king = document.getElementsByName("king").length

	
	window.open("chart.html?"+pawn+
							"&"+bishop+
							"&"+knight+
							"&"+rook+
							"&"+queen+
							"&"+king)
	
	// document.write("<div><canvas id="myChart"></canvas></div>")

	
}












function timer()

{
	var button = document.getElementById("timerBtn"), timer = document.getElementById("timer");

	// alert(button.value)
	
	switch(stoptime)
	{
		case true:
			if(timer.value == 0)
				timer.value = 45;
			button.value = "Стоп";
			stoptime = false;
			startTimer(timer);
			break;
		case false:
			button.value = "Старт";
			stoptime = true;
			break;
		
	}
	
}

function resetTimer()
{
	var timer = document.getElementById("timer")
	timer.value = 0;
	stoptime = true;
	
}

function startTimer()
{
	var button = document.getElementById("timerBtn"), timer = document.getElementById("timer");
	if(stoptime != true)
	{
		
		if (timer.value == 0)
		{
			alert("Time out!");
			button.value = "Старт";
			stoptime = true;
			return
		} 
		
		timer.value -= 1;
		setTimeout("startTimer()", 1000);
			
	}
	
}

function validateForm(event)
{
	var frm=document.forms["UserRegistration"];
	var name=frm.UserName.value, 
		pass=frm.Pwd.value, 
		age=frm.Userage.value,
		level=frm.Userlevel.value;

	let tomatch = /^[a-zA-Z0-9`\-]+$/;
	let vpass = /^[a-zA-Z0-9`\-]+$/;
	
	// alert(tomatch.test(name))
	if(name==null || name=="")
	{
		alert("First name must be filled out!");
		return false;
	}
	// console.log(tomatch.test(name));
	// return false;

	if(!tomatch.test(name))
	{
		event.preventDefault()
		alert("Name doesn`t match template!");
		return false;
	}

	if(!vpass.test(pass))
	{
		event.preventDefault()
		alert("Password doesn`t match template!");
		return false;
	}

	if(frm.Pwd.value != frm.Password2.value)
	{
		// sfm_show_error_msg("Passwords are not the same", frm.Password1);
		alert("Passwords are not the same!");
		return false;
	}

	if(age < 6 || age > 120)
	{
		alert("The age of the user must be more than 6 and less then 120 years!");
		return false;
	}

	document.write("User: " + frm.UserName.value + "<br>" +
					"Password: " + frm.Pwd.value + "<br>" +
					"User age: " + frm.Userage.value + "<br>" +
					"User level: " + frm.Userlevel.value)
	
	
}